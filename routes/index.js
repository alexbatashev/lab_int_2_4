var express = require('express');
var router = express.Router();
var db = require('../db');

/* GET home page. */
router.get('/', function (req, res, next) {
    db.query('SELECT * FROM services', function (err, opt, f) {
        if (err) {
            res.status(500);
            res.end();
        } else {
            db.query('SELECT * FROM bookings JOIN services s on bookings.service = s.id', function (err, data, f) {
                if (err) {
                    res.status(500);
                    res.end();
                } else {
                    var lastH = 9;
                    var lastM = 0;
                    var table = [];
                    for (var i = 0; i < data.length; i++) {
                        var deltaH = data[i].hour - lastH;
                        var deltaM = data[i].minute - lastM;
                        var durFree = deltaH * 60 + deltaM;

                        var countFree = durFree / 10;
                        if (durFree % 10 !== 0) {
                            countFree++;
                        }

                        for (var j = 0; j < countFree; j++) {
                            table.push({busy: false});
                        }

                        var count = data[i].duration / 10;
                        if (data[i].duration % 10 !== 0) {
                            count++;
                        }


                        for (j = 0; j < count; j++) {
                            table.push({busy: true});
                        }
                    }

                    var deltaH = 19 - lastH;
                    var deltaM = 0 - lastM;
                    var durFree = deltaH * 60 + deltaM;

                    var countFree = durFree / 10;
                    if (durFree % 10 !== 0) {
                        countFree++;
                    }

                    for (var j = 0; j < countFree; j++) {
                        table.push({busy: false});
                    }

                    res.render('index', {table: table, services: opt});
                }

            });
        }
    });
});

router.post('/', function (req, res, next) {
    db.query('INSERT INTO bookings (hour, minute, service, name, phone) VALUE ('+req.body.hour+', '+req.body.minute+', '+req.body.service + ', "'+req.body.name+'", "'+req.body.phone+'")', function (err, data, f) {
        if (err) {
            res.status(500);
            res.end();
        } else {
            res.redirect('/');
        }
    })
});

module.exports = router;
